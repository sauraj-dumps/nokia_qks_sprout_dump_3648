## qssi-user 12 SKQ1.210821.001 00WW_2_350 release-keys
- Manufacturer: hmd global
- Platform: holi
- Codename: QKS_sprout
- Brand: Nokia
- Flavor: qssi-user
- Release Version: 12
- Id: SKQ1.210821.001
- Incremental: 00WW_2_350
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Nokia/Quicksilver_00WW/QKS_sprout:12/SKQ1.210821.001/00WW_2_350:user/release-keys
- OTA version: 
- Branch: qssi-user-12-SKQ1.210821.001-00WW_2_350-release-keys
- Repo: nokia_qks_sprout_dump_3648


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
